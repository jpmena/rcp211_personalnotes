# 3/29
* rappel de _16/39_ du cours 9
> Non. La mise à jour de G ne porte que sur les paramètres θ de G. Le gradient ∇θ Lg est calculé par rétropropagation dans D puis G, mais seuls les paramètres de G sont mis à jour.

# 8/29 PatchGan (1 question au prof)
* un site complet sur [PatchGAN](https://paperswithcode.com/method/patchgan)
  * un [lien vers le code](https://github.com/znxlwm/pytorch-pix2pix/blob/3059f2af53324e77089bbcfc31279f01a38c40b8/network.py#L104)
  * un [lien vers le papier](https://arxiv.org/abs/1611.07004v3)
* dans [le code GitHub](https://github.com/znxlwm/pytorch-pix2pix/blob/3059f2af53324e77089bbcfc31279f01a38c40b8/network.py#L104) le discriminateur
  * sort juste une sigmoIde résultat de plusieurs convolution. Chaque sortie est liée à une zone NxN de l'image de départ ?
  * où dans le code fait-on *D optimise la moyenne des entropies croisées sur toutes les prédictions (i, j)*
# 11/29 (Question au prof)
*Q1  D(G(x, z)) est elle la sortie *la moyenne des entropies croisées sur toutes les prédictions (i, j)* ?
  * ce n'es pas dans le code Gitub
> Implicitement si. La sortie du discriminateur (sortie d'une Conv2D + Sigmoid) est un tenseur (1, w, h) où (w,h) dépendent de la dimension de l'image d'entrée. La fonction de coût appliquée (BCELoss) calcule par défaut la moyenne sur tous les éléments.
* on applique en sortie de la sortie la [BCE LOSS lien intéressant](https://towardsdatascience.com/understanding-binary-cross-entropy-log-loss-a-visual-explanation-a3ac6025181a)
> Attention cependant, le code n'est pas l'implémentation des auteurs, qui se trouve [ici](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/tree/master).
* D travaille sur la Sortie de G qui est une image $\~{x}$ ? (réponse p 12/29 avec le Cycle GAN)
   * que signifie *D(y)* ?
> Dans le cas de CycleGAN, pour simplifier les notations, on considère que les images peuvent venir de deux domaines : les images x du domaine A et les images y du domaine B. D(y) est donc le résultat de l'application du discriminateur D sur une image y du domaine B.
# 12/29
* comme expliqué sur cette [Introduction au Cycle GAN](https://machinelearningmastery.com/what-is-cyclegan/)
  * *That is, a large dataset of many examples of input images X (e.g. summer landscapes) and the same image with the desired modification that can be used as an expected output image Y (e.g. winter landscapes).*
  * Y est une image traduite de X qui n'est pas obligatoirement un $\~X$
  * G X $\rightarrow$ Y
  * F Y $\rightarrow$ X

# 13/29
* Explique bien que X et Y sont 2 images différentes mais une est la traduction de l'autre
  * X paysage en été
  * Y le même paysage en hiver
* cylce de gauche vers droite G X $\rightarrow \~Y$ et  F $\~Y \rightarrow \~X$
* cycle de droite vers gauche F Y y $\rightarrow \~X$ et  G $\~X \rightarrow \~y$
* la différence entre x et $\~x$ ou entre y et  $\~y$ s'appelle _cycle consistency loss_
* c'est dans le slide la seconde Loss à prendre en compte ($L_{cyc}(G,F)$) qui est
 * la somme des 2 _cycle consistency loss_
 * La première Loss est la Loss traditionnelle du GAN ici $\~X$ est remplacé par $\~Y = G(X)$ et X par Y

# 16/29 (1 Question)
* Q2 Z est bien le bruit que l'on envoie au Générateur pour tromper le Discriminateur ?
> Oui.
  * en quoi z a t'il pour dimension Z (dimension su bruit) ou bien ce que l'on envoie au dicriminateur est l'espace latent de 'l'encoeur ?
> Je ne comprends pas la question. z est un code tiré aléatoirement dans un espace Z, dont la dimension est un hyperparamètre fixé arbitrairement.
  * Z est beaucoup plus petit que la taille de l'image
> Oui
  * la dimension de W est plus petite que la dimension de Z ?
> En général non, w est de même dimension que z.
    * si oui la partie de droite est un ensemble de Déconvolutions ?
> La partie de droite du schéma (synthesis network) correspond au générateur, qui est effectivement en général un réseau déconvolutif. Même si, en pratique, la "déconvolution" est implémentée en cumulant un [upsampling](https://medium.com/analytics-vidhya/downsampling-and-upsampling-of-images-demystifying-the-theory-4ca7e21db24a) + une convolution classique.
# 17/29
* le professeur a confirmé que $\mu$ = $\frac{1}{m} \times \sum_{i=1}^m x_i$
  * et que $\sigma$ = $\sqrt{\frac{1}{m} \times \sum_{i=1}^m(x_i -\mu)^2}$
  * sachant m la dimension du vecteur x = $(x_1, ..., x_m)$ à l'entrée de l'ADAIN
  * m est donc aussi la dimension en sortie de l'ADAIN ?
* [plus sur les transformations affines](https://www.f-legrand.fr/scidoc/docmml/graphie/geometrie/pytrans/pytrans.html#:~:text=Introduction&text=Effectuer%20une%20transformation%20affine%20(ou,modifie%20la%20valeur%20de%20w.) * la taille de $y_s$ et $y_p$ est la même que la taille de w (but des transformations affines multiplication de W par une matrice de taille w x w) ?
* La taille w de W doit donc être plus grande que celle de l'entrée du dernier ADAIN ?

# 19/29 (1 question)
* Q3 _w = f(z)_ f est la colonne de gauche de du Style GAN ?
> oui
  * les poids de la transformation affine qui donnent $y_{p,i}$ et ceux qui donnent $y_{s,i}$ sont ils appris ?
> oui

# 22/29 (1 Question)
* Q4 VQ-GAN première phrase en rose le décodeur convolutif est G et non D D est le discriminator ?
> En effet.
* Q4Bis: La Loss du GAN ne fait pas intervenir l'encodeur E juste le Générateur (décodeur) G et le discriminateur D ? et Z ?
> La loss du GAN fait intervenir Z, et donc E (puisque c'est l'encodeur qui produit les codes). Donc tous les modèles interviennent dans la loss GAN : E, G et D.

# 26/29
* $\~{y_i}$ est une sortie du classifieur qui travaille sur l'image généré $\~{y_i} = C(G(z_i))$
  * $\~{y_i}$ vaut entre 0 et 1 ($\~{y_i} \in [0,1]$) on en garde que les n $z_i$ qui obtiennent le score le plus haut (notés $z_i^+$)  et les n $z_i$ qui obtiennent le score le moins élevé (notés $z_i^-$)
  * $\vec{Z}$ est la normale à l'hyperplan séparateur

# 28/29
## rappel VAE (1 Question)
* Rappel [VAE CNAM](https://cedric.cnam.fr/vertigo/cours/RCP211/auto-encodeurs-variationnels.html) intéressant pour le passage de la formule de KL versus ELBO
* intéressant est [article WIKIPEDIA](https://fr.wikipedia.org/wiki/Auto-encodeur_variationnel)
  * Q5 question si $p_\theta(x) = \int_z p_\theta(x|z)*p_\theta(z) * d_z$ et que $p_\theta(x|z)$ est une gaussienne (une gaussienne sur z ou sur x en abscisse?)
  * Pourquoi $p_\theta(x) est un mélange de gaussiennes ?
> Pour un z donné, $p_\theta(x|z)$ est gaussienne donc l'ensemble des z $p_\theta(x)$ est un mélange gaussien.

# 29/29 (1 Question)
* Q6 Que veut dire _z = P(x) comme initialisation pour une recherche par descente de gradient_
  * sachant que pour calculer notre loss on prend des $x_i$ générées à partir de $z_i$?
> En apprentissage : on effectue une descente de gradient sur les paramètres du projecteur P à partir du jeu de données (xi, zi) en minimisant || P(x) - z ||.

> En inférence : pour un x dont on cherche le z, on calcule P(x), puis on effectue une descente de gradient sur __z__ de sorte à minimiser || G(z) - x ||.

