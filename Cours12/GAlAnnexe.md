# Dropout
* is based on the  [work of Yarin Gal and Ghahramani](https://proceedings.mlr.press/v48/gal16.html) 16 pages
  * pages de 1 à 4 (sur 10): formulas
* Here I Read the [Gal Supplement material](https://proceedings.mlr.press/v48/gal16-supp.pdf)
  * 20 pages ... 

# 2/20
> to keep notation clean we will write z1 when we mean diag(z1 ) with the diag(·) operator mapping a vector to a diagonal matrix whose diagonal is the elements of the vector
* _diag(z1)_ is a $Q \times Q$ matrix
* _diag(z2)_ is a $K \times K$ matrix
# 3/20
* formula 2: for a classification problem we transform the mean square error loss in a softmax loss
  * more on the [softmax output](https://en.wikipedia.org/wiki/Softmax_function)
  * _converts a vector of K real numbers into a probability distribution of K possible outcomes_
  * $\hat{y_{n,d}}$ is the $d^{th}$ output (d $\in \{1, 2, ..., D\}$) for the $n^{th}$ input point
  * $c_n$ is the index of one in the one hot vector $y_n$ (without hat)
    * $\hat{p_{n,c_n}}$ must be as close to one as possible (take the entire distribution)
    * if it is less than one E can become very positive
* found on google
> Dropout is a regularization technique for neural networks that drops a unit (along with connections) at training time with a specified probability (a common value is ). At test time, all units are present, but with weights scaled by (i.e. becomes ).
* les z1 et z2 sont des matrices $diag(\epsilon_i)$ ($i \in \{1, 2\}$)

# 5/20
* x and y of dimension Q like w 
* b of dimension 1 
## K(X,Y) covariance matrix
* [Covariance Matrix](https://en.wikipedia.org/wiki/Cross-covariance_matrix)
* $K_{X_i, Y_j} = cov[X_i, Y_j] = \mathbb{E}[(X_i - \mathbb{E}[X_i])\times(Y_j - \mathbb{E}[Y_j])]$
## Suite de la page 5
* $w_k$ k from 1 to K column vector $Q \times 1$
* $W_1$ of size $Q \times K$ 
* $\hat{K}(X,X) = \Phi\times\Phi^T$ of size $N \times N$
  * knowing that $\hat{K}(x,y) = \sum_{k=1}^K \sigma(w_k^T\times x)\times \sigma(w_k^T\times y)$
  * _x_ and _y_ 2 vectors of size $Q \times 1$ representing two entries vectors at different times
  * $\Phi(x,W_1,b)$ is not a $1 \times K$ row vector  but a $K \times 1$ column vector...
* I don' know how we arrive to the  product of probabilities of two Gaussian!