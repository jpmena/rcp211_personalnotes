# professeur _clement.rambour@lecnam.net_
# 3/42
* bon résumé de ce que l'on recherche
* en 2 formules
# 4/42
* cosed form: une formule que l'on obtient par calcul...
* Posterior distribution for parameters w is the likehood
# 7/42
* i means the ième point out of N
* k is the k output for that entry out of K
* $\hat{y_{i,k}}=\frac{exp(s_{i,k})}{\sum_{p=1}^K exp(s_{i,p})}$
* Binary case outputs 1 or -1 one dimensional output
  * $p(y_i=1|x_i) = \sigma(s_i)$
  * $p(y_i=-1|x_i) = 1 - \sigma(s_i)$
  * rappel: [$\sigma(x)$](https://fr.wikipedia.org/wiki/Sigmo%C3%AFde_(math%C3%A9matiques))
    $f(x) = \frac{1}{1+e^{-x}}$
# 8 et 9 /42:
* probème de Bernouilli cf. [definition Wikipedia de Bernouilli](https://fr.wikipedia.org/wiki/Loi_de_Bernoulli#D%C3%A9finition)
* $\mathbb{P}(X=x) = p^x \times (1-p)^{1-x}$ avec $x \in \left\{0,1\right\}$
* en effet  $
\mathbb{P}(X=x) = \lbrace
\begin{array}{ccc}
p & si & x = 1\\
1-p & si & x = 0\\
\end{array}$
* dans notre cas $y_i$ est le x de la [definition Wikipedia de Bernouilli](https://fr.wikipedia.org/wiki/Loi_de_Bernoulli#D%C3%A9finition) et vaut 0 ou 1
  * et p est la sortie de notre réseau $\sigma(w^t \times x_n + b)$

# 11/42
* une droite car w = Cte = WMAP et $y_i = \sigma(w_{map}^t \times x_i + b)$

# 12/42
* $p(w|\textbf{X,Y}) \sim q(w)$
* q(w) = $\mathcal{N}(w, \mu, \sigma^2) = \frac{1}{\sigma \times \sqrt{2 \times \pi}} \times \exp{-\frac{1}{2} \times (\frac{x - \mu}{\sigma})^2}$
* une [loi Normale Wikipedia](https://fr.wikipedia.org/wiki/Loi_normale) est une guassienne
* le [mode d'une variable aléatoire](https://www.bibmath.net/dico/index.php?action=affiche&quoi=./m/mode.html#:~:text=Le%20mode%20d'une%20variable,%C3%A9gale%20%C3%A0%20sa%20moyenne%20m%20.) est sa valeur prise le plus souvent
  * donc ici le max sur w de la loi Normale ce qui correspond à la moyenne !!!
  *  défintion de la [loi Normale multidimensionnelle](https://fr.wikipedia.org/wiki/Loi_normale_multidimensionnelle)
*  et de la [matrice Hessian](https://onlinelibrary.wiley.com/doi/pdf/10.1002/9780470824566.app1) 
  * comment [la calcule t'on](https://resources.system-analysis.cadence.com/blog/msa2022-how-to-compute-the-hessian-matrix-of-a-scalar-valued-function)
  * elle est l'inverse de la [covariance matrix](https://en.wikipedia.org/wiki/Covariance_matrix)

# 14/42
* on prend un élément on remplace l'intégrale par une somme sur S éléments (S taille du batch)
  * donc $q(w) \times dw$ vaut 1 et w est sampled dans la distribution q(w) c'est $w^s$
  * manque $\frac{1}{S}$  car c'est la moyenne que l'on calcule!

# 15/42
* __Option 2__ _binary case_: C'est toujours $p(y=1|x^\ast,D)$
* approximage $\sigma$ par probit $\Phi$ et le produit de $\Phi \times \mathcal{N} = \Phi$
  * du pur calcul, le professeur n'a pas insisté

# 18/42
* |x,w est similaire à $|f^w(x)$
* dans le produit de gaussienne $p(y_i|x_i,w)$ a pour moyenne $f^w(x_i)$ qui n'est pas linéaire en xi car il y a des RELU et un $\sigma$ 
## partie RECAP
* Quand le produit de gaussienne est il une gaussienne ?
* le p(x) correspond au p(w) = $\mathcal{N}(w|0, \alpha^{-1} \times I)$
* le p(y|x) correspond au $p(y_i|x_i,w) = \mathcal{N}(y_i|f^{w}(x_i),\beta^{-1})$ (pas de matrice ici y est de dimension 1x1) Bernouilli)
* le p(x|y) correpond au $p(w|x_i,y_i, \beta)$
### quand Gaussienne ???
* p(y|x) doit être une normale avec une moyenne en expression linéaire $\mathcal{N}(y_i|w^t\times x_i +b,\beta^{-1})$

# 19/42 
* Noté pendant le cours __MCMC on ne regarde pas cela ici__
* [pseudo code ce Metropolis](https://fr.wikipedia.org/wiki/Algorithme_de_Metropolis-Hastings#Pseudo-code)

# 21/42 (1 Question)
* on suppose que le posteriori est le produit de tous les (Variational approximate posterior)
  * $q_\theta(w) = \mathcal{N}(w|\theta) = \mathcal{N}(w|\mu,\Sigma)  = \prod_{j=1}^D \mathcal{N}(w_j|\mu_j,\sigma_j)$
  * D représente le nombre de poids dans le réseau, on itère sur tous les poids
  * Q1: $\mu$ et $\Sigma$ sont des scalaires ?
  * Q1Bis: q(w) est une fonction scalaire w est un scalaire ? 
    * c'est $\prod_{j=1}^D \mathcal{N}(w_j|\mu_j,\sigma_j)$ ?
    * ou bien $\prod_{j=1}^D \mathcal{N}(w|\mu_j,\sigma_j)$ ?

# 22/42
* [Kullback Leibner](https://fr.wikipedia.org/wiki/Divergence_de_Kullback-Leibler)
  * P représente une distribution de probabilités
  * Q représente une théorie !!!
* La seconde ligne vient de la démo de la page 18/43 du [Cour11.md](../Cours11/Cours11.md) 
  * c'est la réponse que m'a faite _marc.lafon@lecnam.net_
* dans la 3ème ligne on sort _log p(Y|X)_ car $\int q_\theta(w) \times dw = 1$ car $q_\theta(w)$ est une distribution normale de probailité
  * produit des distribution normale de chacun de $w_j (\mu_j, \sigma_j)$ 
## nota Bene
* le rappel de la la démo de la page 18/43 du [Cour11.md](../Cours11/Cours11.md)
* est dans le cartouche en bas et à gauche

# 24/42
* p(Y|X,W) = $\prod_{i=1}^N p(y_i|f^w(x_i))$

# 25/42 (1 question)
* Q2: Comment calcule t'on $q_\theta(w)$ connaît t'on tous les $\sigma_j et \mu_j$ j allant de 1 à D ?
  *  on diag()parle dans le slide de solution analytique pour $KL(q_\theta(w) || p(w))$
* Q2Bis:  $KL(q_\theta(w) || p(w))$ minimal veut il dire que $q_\theta(w)$ ressemble au prior (théorique) ?
  * ne veut t'on pas s'en éloigner
# 26/42 (1 question)
* A continuer ...
* on prend $\int q(w) \times dw$ en faisant la moyenne sur 1 w celui du point i !!!
* S est le batch au lieu de $\sum_{i=1}^N$ on fait $\sum_{i=1}^S$ 
*  Q3: le $w_j$ n'est pas le $w_j$ (j de 1 à D) de cacun des paramètres ne devraît être pas un $w_i$
* Q3Bis: est ce que ce sont ces $\mu_i$ et $\sigma_i$ que l'on calcule par descente de gradient ?
# 27/42 (1 question)
* Q4: $\theta=(\mu, \sigma) ?
* Q4Bis: Comment calcule 't'on $\frac{\partial}{\partial{\theta}} KL(q_\theta(w)||p(w))$ forme analytique simple?
  * c'est un KL de 2 distributions: $\mathcal{N}$
# 28/42 (1 question)
* Q5: La Hessian A de dimension DxD ? elle contient les dérivées partielles de $f^w(x)$ sur $w_i, w_j$
*  que contient g  les dérivées partielles: $\frac{\partial}{\partial w}\hat{\Delta\theta}$ (quel est le gradient)? 
# Dropout
* basée sur le [travail de Yarin Gal et Ghahramani](https://proceedings.mlr.press/v48/gal16.html) 16 pages
  * pages de 1 à 4 (sur 10): formules 
# 31/42 Dropout (le professeur est passé très vite dessus)
* x de dimension (1,D) _précisé en bas et gauche_, h de dimension (1,L) _même dice n'est pas écrit_
  * tous les 2 sont des vacteurs ligne !!! 
* W de dimension (D,L)
* $\epsilon_i$ i de 1 à D à 1 avec une [probabilité de Bernouilli](https://fr.wikipedia.org/wiki/Loi_de_Bernoulli#D%C3%A9finition) de (1-p) pour x=1
* probabilité à 0 de p: $\mathbb{P} (\epsilon_i = x) = p^{1-x} \times (1-p)^x$ avec $x \in \{ 0,1 \}$
* $\hat{\epsilon} = \{\epsilon_i\}$ vecteur ligne de D $\epsilon_i = BERNOUILLI(1-p)$
# 32/42
* de même $\hat{W_1} = diag (\hat{\epsilon}) \times W_1$
  * car $\hat{x} = x \times diag (\hat{\epsilon})$: comme montré dans le slide
  *  $diag (\hat{\epsilon})$ de taille _D x D_
*  sortie de $h = \sigma(x \times \hat{W_1} )$ h de dimension (1,L) $\sigma$ introduit une non linéarité

# 34/42 (1 question)
* Q6: J'ai trouvé nulle part dans le cours pourquoi:
  * $||f^{\hat{w}}(x_i) - y_i||^2 = - \frac{1}{\tau} \times log(p(y_i|f^{g(M,\hat{\epsilon}^i)})(x))$ ?
    *  que vient faire x ici ?
## 34/42 (Question subsidiaire)
* Q6biss: qu'est ce que $\hat{\epsilon^i}$ ce n'est pas le produit des  matrices $\epsilon_l$ l valant 1 et 2 ?
# 35/42
*  Q7: C'est quoi l'algo 1 quelle page ?