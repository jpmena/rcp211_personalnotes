## Slide 8 (10/55)

* Attention, dans la boucle il y a 2 expressions
  * N <-N+1
  * V <- V + (1/N) (G - V)
* et on le parcourt pour tout St
* Est ce que les membres à gauche de l'expression sont la valeur à t+1 ?
* __<span style="color:red">pourquoi note t'on à gauche la dépendance à Pi et pas à droite ?</span>__
* Que signifie non stationarité ?
  * réponse Google _Si la structure reste la même, le processus est dit alors stationnaire._

## Slide 11 (15/55)

* TODO: revoir L'équation de BellMan Gt est il une espérance ?
  * Bellman: Gt = Rt+1 + Gamma * vpi(St+1)
  * Bellman vPi(s) = Espérance de valeurs en t+1 ?

## Slide 14 (21/55)

* Je ne comprend pas le cas pratique

## Slide 20 (27/55)
* Gt(n) a été définit Slide 12 !!!

## Slide 23 (31/55)
* Je ne sais plus ce qu'est qpi
  * réponse se référer au slide 33 du cours 1 !!!!
  * qpi proche de v

## Slide 25 (34/55)
* __<span style="color:red">que signifie le 1 en grand ?</span>__

## Slide 26 (36/55)
* il y a bien (m-1)*Epsilon/m actions non optimales
* et la solution optimale avec une probabilité correspondante (pour que la somme des m actions possibles fasse 1)

## Slide 27 (37/55)

* __<span style="color:red">que signifie le greedy(Q) ?</span>__

## Slide 30 (41/55)

* is 1..k an episode ?

## Slide 35 (48/55)

* __<span style="color:red">Je ne savait pas que G pouvait s'écrire comme un produit de Pi</span>__