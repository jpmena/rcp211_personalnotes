# Slide 18/45
* on utilise le terme Gaussian comme Normal comme expliqué dans ce [post Stackexchange](https://stats.stackexchange.com/questions/55962/what-is-the-difference-between-a-normal-and-a-gaussian-distribution)
 * Normal serait Gaussian avec variable z = (x - mu) / std
 * ce qui signifie une gaussienne de moyenne 0 et d'écart type 1
# Slide 20/48
* in [DKL](https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence)
  * Q the denominator is the reference probability
* N, 0, I I is the Identity matrix of dimension d
* je ne comprends pas ce que je cherche si la moyenne est à 0 et la standard deviation I

# Slide 22/43
> L'expression analytique de la KL page 11/19 (ou 22/43) n'est vraie que
si p(z) ~ N(0,I) et q(z|x) ~ N(mu, sigma).

# Slide 24/43 Inférence
## 2 questions à poser:
* Je croyais que z suivait un loi Normale (0,I)
> La section "Inférence variationnelle" décrit un cadre plus général que
le VAE, donc on ne fait plus d'hypothèse ici.
* pourquoi z et x|z suivent la même loi de paramètres $\theta\ast$
> z et x|z ne suivent pas la même loi, en revanche elles dépendent toutes
les deux des paramètres theta de la loi a priori p_\theta(z). C'est
juste un raccourci de notation.

# Slide 25/43
* explique le lien entre z et x
  * là où il y a z, il y a x
  * z [loi de Bernouilli](https://en.wikipedia.org/wiki/Bernoulli_distribution), ne prend donc que 2 valeurs:
    * vaut 1 avec une proabilité p
    * vaut 0 avec une probabilité 1-p
* les gaussiennes autour de ces valeurs sont les probailité de trouver
  * des x pour z = 1
  * des x pour z = 0
## Slide 26/43
* la formule $p\Theta(x) = (1 - p)p\Theta(x|0, \sigma) + p * p\Theta(x|1, \sigma)$
  * est celle que l'on retrouve sur le slide suivant sous $\int$
# Slide 27/43 (question à poser)
* pourquoi se pose t'on la question de $p\theta(z|x)$ quand ce qui nous intéresse est $p\theta(z)$
> p(z) nous intéresse beaucoup moins que p(z|x) ! p(z|x) nous permet de
relier les variables latentes aux observations, au travers du modèle
p_\theta(z). Si l'on fait de la prédiction (ce qui est permis par
l'inférence variationnelle au sens large, même si ce n'est pas ce que
l'on utilise dans RCP211), on aura besoin de p(z|x) par exemple.

# 30/43 (Questions à poser)
* maintenant $q\phi(z|x)$ n'est plus une somme moyenne de gaussienne autour des points xi ?
* la vraisemblance c'est L'ELBO ?
* c'est OK ce n'est pas une question
  * p 28/43 on parle de $KL(q\phi(z|x)|p\theta(z|x))$
  * p 30/43 on pale de $KL(q\phi(z|x)|p\theta(z|x))$

# 31/43 (Questions à poser)
* p 21/43 on avait déjà pris pour p(z) une N(0,I) je ne sais plus pourquoi on peut ramener z à une normale
  * c'est bien la moyenne de p(z|xi) qui sont des gaussiennes autour de xi ?
> Tout dépend de si vous parlez de p(z) ou p_\theta(z). Mais dans
l'absolu, par définition des probabilités conditionnelles, p(z) est
égale à l'espérance sur les x_i de p(z|x_i). En revanche, les p(z|x_i)
ne sont pas des gaussiennes. Ce sont les distributions postérieures,
c'est-à-dire les approximations q(z|x_i) qui sont gaussiennes.

# 33/43
* Question: je croyais que p(z) = N(0,I)
> Oui, mais pendant l'entraînement on échantillonne dans la distribution postérieure q(z|x) pour un x donné.

# p 41/43
* Dans le cours du dessus D(x)[0] première coordonnée pour la sortie du Décodeur de x: $\mu(xi)$
* Dans le cours du dessus D(x)[1] seconde coordonnée pour la sortie du Décodeur de x: $\sigma(xi)$
<img src="images/Cours_07.jpg" alt="le Cours de ce 08/11/2023" width="1000" />

# p43/43 (Question à poser Réponse partielle trouvée en TP)
* $p\theta(z) est elle toujours une N(0,I) ? Où est cela vraie valeur que l'on essaie de trouver
  * Comment on construit le KL ? pour calculer le _L_
  * réponse donnée dans l'[énoncé du TP7](https://cedric.cnam.fr/vertigo/cours/RCP211/TP7-AEetVAE.html#chap-tpvae-1)
  * également sur la _22/43_
* Est ce que cette formule ne s'applique que si p(z) = N(0,I)
> L'expression analytique de la KL page 11/19 (ou 22/43) n'est vraie que si p(z) ~ N(0,I) et q(z|x) ~ N(mu, sigma).

> L'équation p43 est générale et vaut pour tout a priori.

* Pourquoi cet à priori ?

> Parce qu'on ne peut pas faire les calculs sans donner une expression pour p(z). À défaut d'autre chose, on choisit N(0,I).

## Erreur de reconstruction et ELBO

### Réponse de Nicolas Audebert

> La fonction objectif du VAE *est* la ELBO !

> Par définition : ELBO(qϕ)= Eqϕ(z|x)[logpθ(x|z)]−KL(qϕ(z|x)||pθ(z))

> Le premier terme correspond à la vraisemblance du modèle, ce qui est
équivalent à l'erreur de reconstruction. Le second terme est la
divergence KL entre le posterior et le prior.

> Et que trouve-t-on dans VAE loss ? L'erreur de reconstruction entre x et
x̂, et la formule analytique de la divergence entre le posterior et le prior.

> C'est donc la même chose, heureusement.

> Bonne journée,

> Nicolas Audebert

### Réponse de George le Bellier

>  En effet, nous n'avons pas réellement eu le temps d'en parler lors du TP de la semaine dernière. Je joins à ce mail le détail du calcul qui permet de faire le pont entre la ELBO et la loss utilisée dans le TP.
Bonne journée,
Georges Le Bellier

* cf. Le PDF sous Docs
* il dit dans notre TP pour le premier terme on utilise une [binary cross entropy](https://towardsdatascience.com/understanding-binary-cross-entropy-log-loss-a-visual-explanation-a3ac6025181a)
* il dit bien  _ce qui rejoint le code propos ́e dans le TP o`u on cherche `a minimiser −L(θ, φ; x)._

### Autre question

* cf. [lien Wikipedia VAE](https://fr.wikipedia.org/wiki/Auto-encodeur_variationnel)
  * _Comme fonction de coût pour la reconstruction, l'erreur quadratique moyenne et l'entropie croisée sont souvent utilisées. _
  * qui me renvoie vers l'[entropie croisée Wikipedia](https://fr.wikipedia.org/wiki/Entropie_crois%C3%A9e)
#### Entropie  croisée
* _distribution des événements est basée sur une loi de probabilité q q, relativement à une distribution de référence p p._
* ici q = pθ(x|z)
  * et p = qϕ(z|x)
* minimiser la LOS est aller vers -inf ? (cf. le log)
* ne manque t(l pas un signe - à ELBO(qϕ)= Eqϕ(z|x)[logpθ(x|z)]−KL(qϕ(z|x)||pθ(z))
  * fin du PDF il dit bien _ce qui rejoint le code propos ́e dans le TP o`u on cherche `a minimiser −L(θ, φ; x)._
