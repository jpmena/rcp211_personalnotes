# 6/28
## Cook and Weisberg
* cela nous renvoie vers [le livre de Cook and Weisberg de 1982](https://conservancy.umn.edu/handle/11299/37076)
  * c'est un livre de 240 pages ce n'est pas un papier
* [seems to be the paper, A Note on an Alternative Outlier Model](https://rss.onlinelibrary.wiley.com/doi/abs/10.1111/j.2517-6161.1982.tb01215.x)
  * but you must pay
* Je ne comprends pas ce slide. Je n'étais pas là
# 7/28 (je suis arrivé quand il terminait ce slide) (1 Question)
* Je ne comprends pas les schémas
* Je préfère le schéma [p 5/21 de ce papier](https://www.mdpi.com/1424-8220/23/11/5273)
* (Q1) $\Pi Model$: pourquoi $z_i$ est il passé par la Stochastic augmentation, ne devrait être pas uniquement $\tilde{z_i}$
* (Q1Bis) Temporal Ensembling: $\tilde{z_i}$ est un paramètre d'entrée comment est il obtenu?
  * $z_i$ doit il être la sortie avec de la stochastic augmentation ?
# 8/28
* minimiser sur des paramètres du réseau telle que l’erreur max soit la plus petite…
  * on prend la cross entropy dans le cas de classes …
* Je suis robuste dans cet ensemble
# 9/28
* [Logistic regression](https://web.stanford.edu/~jurafsky/slp3/5.pdf) notamment p 3/25 qui explique les poids (une seule couche et pas de RELU) shéma 5.2 $\rightarrow$ 5.3
* La Loss est maximale quand $w^t \times x^{(i)} + b$ et $y^{(i)}$  sont contradictoires
 * l'un vaut 1 et l'autre -1
 * donc le max de la Loss est le min du produit (2 réels)
## 11/28
* project on the domain, clamp the value:
*   * voir la documentation de [Torch.clamp](https://pytorch.org/docs/stable/generated/torch.clamp.html#torch.clamp)
```python
delta = torch.zeros_like(pig_tensor, requires_grad=True)
opt = optim.SGD([delta], lr=1e-1)
// ..............................
delta.data.clamp_(-epsilon, epsilon)
```
* $\delta$ est le même pour tous les $x^{(i)}$ donc on peut tester  avec des $x^{(i)}$ différents
* $\Delta$ est le domaine $-\epsilon +\epsilon$

# 24/28
* [Rademacher complexity](https://en.wikipedia.org/wiki/Rademacher_complexity)