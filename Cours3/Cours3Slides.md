# Slide 11 (12/54)

* __<span style="color:red">On parle de Esparance sur Pi</span>__
* que signifie échantillon ?
## 12:
* on suit une politique Pi
* Esparance des vpi se fait sur une seule politique et après on met à jour les paramètres
* Où se situe le choix d'un échantillon on ne fait pas pour Delta w une moyenne sur un échantillon d'états ?

# 14 (15/54)
* x1(s) chf dans le TP état_avant
* x2(s) cf. dans le TP droite ou gauche etc...
* vu en cours v chapeau est comme vpi c'est un scalaire
Le Delta inversé peut être sortie de lespérance ca il n'est pas fonction de Pi
# 15 Monte Carlo
* Pourquoi a t'on un -2 * () et non 2 * ()

# 16
* répond à des questions
* que sont les Li ?
* cas k=1
  * que vaut s11, s12, s13 .... s1L1

# 18
* En bas c'est r' et non r ar plus haut c'est rt+1...
# 19
* on remplace rk+1 par rk
* où apparaît ak ?
  * comme est construit x(s) quel lien avec le _Sample tuple (sk, ak, rk,sk+1)_
  * est ce qu'ici on ne prend pas en compte a cf slide _21_

## Slide 22 (25/54)
* la dérivée de la Loss sur w n'est pas 1/2*E mais -2*E

## Slide 23 (26/54)
* Approximation avec SARSA, on n'a plus de Pi
  * Erreur d'écriture par
* Le gradient de la Loss
  * devrait être -2*()
  * on ne dérive pas W(s',a',w) pourquoi ?

## 24 (27/54)
* SARSA cas linéaire car Q est un produit scalaire avec le vecteur w (à déterminer de même taille n que x(s,a))
* mêmes questions que pour le slide précédent

## 25
* le maxa' ne se fait que si on est dans le cas Epsilon (très faible) ?  Et pour les autres ?

## 32 Depp RL

* Je me demande toujours ce qu'apporte la convolution (à réviser)

## 34
* on ne dérive pas sur s',a' ? pourquoi ? Question déjà posée dans le cas linéaire ...
* pourquoi __max a'__ alors que cela ne se fait que dans Epsilon des cas

## 35
* cf. TP3 pour le nom __target (cible) que l'on ne change pas__ et le __updated (réseau mis à jour)__
  * là je comprend pourquoi on a dans le __TP3 2 réseaux PyTorch__

## 36 algortihme

* <span style="color:red">la grosse question __maxa'__ alors que l'on ne prend le max que Epsilon du temps</span>

## 38

* Que sinifie fixed Q, replay Q

## 41

* plus Beta est grand, plus la distribution de probabilités pi est pointue

## 43
* __Quelle différence entre Qpi et vpi si tous les deux dépendent de a?__
  * TODO revoir Qpi et son calcul
