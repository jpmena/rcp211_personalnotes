# Comes from [DavidSilver Courses](https://www.davidsilver.uk/teaching/)

## [Markov Decision Process](https://www.davidsilver.uk/wp-content/uploads/2020/03/MDP.pdf)

### Slide 10 Pss'

* The lines are the start states (s)
  * The sums of the probailities on the lines must be 1
* the columns are the target states (s')
  * The sum of the probailities on the columns ahas no importance

#### About the terminal Sleep state:

* that state is terminal from there you cannot go anywhere
* By convention it can only go to hmself with a probability of one!!!

### Slide 12 Markov Reward Process

* An example is at the following slide
* It seems the Reward is a vector of _number of States_ Elements
* for exemple for Class1 (s1) it is _0.5 * -2 + 0.5 * -1_
  * The rewards are on the target states
* for exemple for Class 3 (s3) it is 0.6 * 10 + 0.4 * 1
* Rs: the _s_ indicate that it is a vector of values related to the different states
* in the same way Pss' indicates it has two dimensions related tho the different states as source states _s_ (lines) and the same states as possible target _s'_ (columns)

### Slide 14 the Return Gt

* values immediate reward above delayed reward
* it is a scalar (depends only of the way you are using)
* Example 3 slies after because it is related to v(s) which is a ector made of v1

### Slide 16

<img src="img/network.jpg" alt="The network of States with their rewards" style="height: 600px; width:1000px;"/>

* The calculation of the different pathes leading to the blocking state __(Sleep)__
* The esperance is a vector of the different states:
* each vector's value of _i_ indice is the mean value of __Gi__
* the example which is given on the next slide, shows how we calculate the different G1 (only 4 pathes possible from state _Class1_ to the final state _Sleep_)
  * so that our  __v[1] = 1/4(-2.25 + -3.25 + -3.41 + -3.20)__

### slide 18

* _Gamma = 0_: I take ino account only my own Reward independently of the path

### slide 20 _18 out of 57_

* Gamma = 1 : I take into account every one
  * We have a loop problem with the state _FaceBook_
* our __v[1] = 1/n(4 + -8 + 1 + x + y)__

### slide 21
* t+1 is a successor state (see caculation of Gt)
* for state 1 (_Class1_) it is either _Class2_ or _Facebook_
  * so the Esperance change it is now a mean on the _Successor states + their corresponding rewards_
  * 1/2(Rclass2 + Gamma*v[Class2] + Rfacebook + Gamma*v[Facebook])
  * __|St=s__ says that the current or starting state is in our case _Class1_

### slide 22

* lines of Pss' are the entries of s!
* nex slide is a good illustration

## slide 23

* Here Gamma = 1
* 4.3 id the new values of the red position at step 2 (step 1 is the origin)
  * which is __Class 3__ see ![the orginal map](files:img/network.jpg)
* next slide expresses it even better

## slide 24

* The only question is what are the initial values (see slide 23)

# The MDP

## slide 27

* a MDP is a MRP with an action out of a finished set of actions (A) in more.

## slide 28
* The actions' names are in RED!
* Actions and States live in two differents domain names! (Facebook for actionis different from the Facebook state name).
* We see that the State __Facebook__ has 2 Rewards,
  * one with the _FaceBook_ action, value -1
  one with the _Quit_ action, value 0
* we see one action _Study_ which affects the Rewards of 3 states _Class 1_, _Class 2_, _Class 3_, _Pass_
* One question remains
  * Why is the previous Pub state a black point
  * There is no reward outgoing from it

## slide 29
* Pi is a probaility that is between 0 and 1
* My question how do you calculate it concretely?
  * on the slide 32 its value is fixed to 0.5 for At=a and St=s
  * on the slide 32 At is of size 5: [Facebook, Study, Sleep, Quit, Pub]
  * on the same slide 32 St is of size 5 or 6 (is the back point a state ?)

## slide 31 (28/57)

* The Esperance (mean) following Pi has been presneted at Slide 20
* The difference between v and q is that with q you starts with action a!!!