# Slide 5
* JE ne me souviens plus ce que veut dire VpiTheta
* se référer au [notes de fin de cours](https://jhub3.cnam.fr/user/24592/lab/tree/MonDossier/RCP211/TP5/Cours5.md)
* Yannis en parle [Note de TP5](https://jhub3.cnam.fr/user/24592/lab/tree/MonDossier/RCP211/TP5/TP5.md)
  * mais n'éclaircisse pas
# Slide 9
* Qu'est ce que T(Tau) ? Pourquoi est ce égal à G0?

# SLide 10
* a été développé en fin de cours par le prof [notes de fin de cours](https://jhub3.cnam.fr/user/24592/lab/tree/MonDossier/RCP211/TP5/Cours5.md)
# Slide 11:
* grande variance car nécessité de parcourir complètement (cf. tP4) avant de mettre à jour les paramètres
* c'est ce que Yannis explique dans ses [Note de TP5](https://jhub3.cnam.fr/user/24592/lab/tree/MonDossier/RCP211/TP5/TP5.md)

# Slide 12:
* 1ère formule Pourquoi remplace t'on R(t) par G(t) 
  * est ce que l'on suppose que Gamma vaut 1 ?
*  bas de slide: TODO voir d'où vient l'erreur TD (cours précédent)

# slide 13 cf. le TP5
* nous oblige à optimiser 2 paramètres du modèle
  * le Critique w
  * l'actor Theta
# Slide 15:
* Quelle différence entre le offline Actor critic
* Et le Online actor critic
  * pourquoi dit t'on que dans ce dernier cas le batch est de taille 1 (il y a bien des i)
* Que signifie _Fit Vpi vers le TD Target_ ? on le fait de 0 à t ?
* Le TP c'est le OffLine ou le Online ....
# Slide 18
* Qu'appelle t'on la __KL divergence__ ? formule incompregensible ...
  * [Divergence de KL](https://fr.wikipedia.org/wiki/Divergence_de_Kullback-Leibler)
  * qu'est ce que Z ?
* __TODO__ voir le TP ...

# Approche basée modèle:
## Slide 29
* [environnement stochastique](https://rlearn.fr/environnement-stochastique-et-deterministe/)

## Slide 35 (36/41)
* algorithme pas simple ....
## Slide 36
* Ct x (vecteur colonne) --> vecteur colonne
  * donc le coût est bien un scalaire
## Slide 37
* __Question: <span style="color:red">Que sont Qt par rapport à qt et Vt par rapport vt et Ft par rapport à ft</span>__
## Slide 38
* que sont s' par rapport à s
* ce sont tous des gradients doubles ?
  * dans tous les cas comment calcule t'on ces gradients?
