# si cela ne s'affiche pas bien
* fermer et réouvrir
# il y a 2 modèles
* on avait un réseau de Neurones qui optimisait Q 
* Reinforce un réseu de neuronne qui en fonction d'une action 
* Critic At erreur TD0 on remplace Gt par At (temporal difference du TD0 du TP2) Loss du TP3 !!! pour estimer le DQN
* Actor cf. TP précédent où l'on remplace Qt par At que l'on a à chaque t.
## LOSS du critic
* L = V(s[t]) - (r[t] + Gamma * V(s[t+1]))
* entre la couche d'entrée et la couche de sortie manque la couche RELU

## LOSS de l'acteur
* Voir TP4 où l'on remplace Gt par At

## nombre de caractéristiques d'état
* ce n'est pas la voiture ici c'est un pendule
* les valeurs de sortie sont entre -2 et +2 de là le 2x

## Sortie de Actor
* [Softplus](https://pytorch.org/docs/stable/generated/torch.nn.Softplus.html) juste pour que l'écart type soit positif proche du RELU
* question: pourquoi passe t'on par le log de l'écart type -qui peut être très négatif ? (Question que se pose Yannis, il va demander à Clément cet )
* car la couche peut prendre de valeurs entre -Inf et + Inf on considère que c'est donc un log d'écart type
  * le RELU pour apprendre des fonctions non linéaires ....
## Notes prises sur le TP 5
* à scanner et mettre ici !!!

## un bonne explication de Yannis
* Ce schéma au tableau:
<img src="images/NotesTP1_1.jpg" alt="Les Notes de Yannis" style="height: 600px;"/>
* Il y explique que :
  * Actor Critic est plus avantageux, plus stable car on n'attend pas la fin de l'épisode pour avoir le Reward et donc mettre à jour les coefficients du réseau de neuronnes
  * A chaque Step de l'épisode on a une estimation du REWARD donc l'optimisation est plus stable
    * en effet on met à jour les coefficients du Réseau de neuronnes à chaque step!