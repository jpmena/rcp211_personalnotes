# Back to Bischop
## 75
* We go back to [Bishop Pattern Recognition and Machine Learning free download from Micrsoft](https://www.microsoft.com/en-us/research/people/cmbishop/prml-book/) on the page 75
* formula 2.28: the sum on x is the sum on all possible x, each one with one 1 and the rest of zeros
  * that makes $(\mu_1,0,0,....,0)^t + (0,\mu_2, 0, 0, ...., 0)^t + ... + (0, 0, 0, 0, ...., \mu_K)^t$ 
* formula 2.29, 2.30: $m_k=\sum_{n=1}^N x^{nk}$ so
  * $p(D|\mu) = \prod_{k=1}^K \mu_k^{m_k}$
  * $m_k$ represent the number of observations for which $x^k$ is equal to one
    * these are called the sufficient statistics for this distribution
* the [Lagrange multiplier problem](https://en.wikipedia.org/wiki/Lagrange_multiplier) states equation 2.31
  * $\frac{\partial{L}}{\partial{\mu_k}}$ and $\frac{\partial{L}}{\partial{\lambda}}$ must be null
* 2.32 $\sum_{k=1}^K m_k = N$ the number of observations for which $x^k$ is one summed on all k
  * so $1 = -N/\lambda$ $\mu_k^{ML} = m_k/N$ (ML for lagrange multiplier)
  * which is the fraction of the N observations for which $x^k$ equals 1 
## 76

