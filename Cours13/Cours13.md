# le professeur est revenu sur le Dropout du Cours 12

# 4/44
* pour 3 classes (K=3) on dessine un triangle sur le plan de dimension K-1 = 2
* $p(y|x^\star, w^s)$ est compris entre 0 et 1 est est la distance
  * du output au dog au wolf au cat
# 5/44
* que signifie [SGD with LR](https://machinelearningmastery.com/understand-the-dynamics-of-learning-rate-on-deep-learning-neural-networks/)
  * _Stochastic Gradient Descent_ with _Learning Rate_
# 6/44
* [MC Dropout](https://paperswithcode.com/method/monte-carlo-dropout): signifie Monte Carlo Dropout
* [bonne explication sur AWS](https://docs.aws.amazon.com/prescriptive-guidance/latest/ml-quantifying-uncertainty/mc-dropout.html)
# 7/44
* [Variation ratio](https://en.wikipedia.org/wiki/Variation_ratio) ici c* est le mode et T le nombre de forward passes (N sur Wikipedia)
## Predictive Entropy
* maximale si tout le monde au même p sur c (du coup p faible -log(p) grand), grand une des moyenne de p dur les T expériences vaut 1 (pour les autres c vaut 0) dans ce cas l'Entropie est proche de 0!
* $\hat{w_t}$ est le calcul des paramètres pour chaque t de 1, 2, ...T
## Mutual information (1 question)
* Elle est définie par [Mutual Information](https://en.wikipedia.org/wiki/Mutual_information)
  * Q1 qui est X qui est Y ici  [définition française sur Wikipedia](https://fr.wikipedia.org/wiki/Information_mutuelle)
# 11/44 (1 question)
* on revient sur [Explication de AWS](https://docs.aws.amazon.com/prescriptive-guidance/latest/ml-quantifying-uncertainty/mc-dropout.html)
* $w^s$ signifie le w tiré de le tilde signifie tiré d'une Gaussienne
  * Q2 cette gaussienne a une moyenne $f(x,\theta)$ et une variance $s^2(x, \theta)$
    * comment les calcule t'on ? en fait dans ici ils sont dit qu'ils sont tirés du MonteCarlo DropOut
    * dans ce cas qu'est ce que cela apporte de dire que le likehood est Gaussian Dristributed?
# 15/44
* Quand on écrit sous le triangle DIR(1,1,100) cela signifie $\alpha_0=1$ $\alpha_1=1$ $\alpha_2=100$ ?
* je laisse tomber jusqu'à la page 21/44 (trop compliqué Dirichlet et pas du tout expliqué par le professeur)
* C'est à la page 75 du Bishop (que j'ai téléchargé au cours 10)

# 21/44
# 30/44
* [le papier de Nicholas Thome](https://arxiv.org/abs/1910.04851) p 4/11 on retrouve le schéma où l'on apprend la confidence.
  * à cette page __classifier__ est un réseau de neuronnes M de paramètres w
# 34/44
* ne détaille pas [ODIN](https://arxiv.org/abs/2002.11297)
* ODIN (d'après le papier) propose 2 méthodes
> provides two strategies, temperature scaling and input pre-processing, to make the max class probability a more effective score for detecting OoD data