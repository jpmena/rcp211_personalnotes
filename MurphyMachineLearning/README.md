# Murphy Machine Learning
## Free Download
* [Murphy_Machine_Learning.pdf Livre 1](https://probml.github.io/pml-book/book1.html)
  * fait 45.7 MB
* there is a Github project for the accompanying code of this book: [GitHub Project](https://github.com/probml)
  * pour le livre 1 cela donne [Livre 1 code](https://github.com/probml/pyprobml/tree/master/notebooks/book1)
## Why this book
* It is remcommended for the five last lessons of the [RCP211 Advanced AI Course at the CNAM Paris](https://formation.cnam.fr/rechercher-par-discipline/intelligence-artificielle-avancee-1132536.kjsp)