# slide 3/44 Rappels
## Référence FreeCodeCamp
* Je retrouve la Q Table sur [freecodecamp](https://www.freecodecamp.org/news/an-introduction-to-q-learning-reinforcement-learning-14ac0b4493cc/)
* nice presentation of a _state/action_ table...
  * ici alpha = 0.1 et Gamma = 0.9
# Slide 7
* _Actor Critic_ peut se produire
  * dans le cas Value Based
  * dans le cas Policy Based
# Slide 14/44
* Qu'est ce que la value, qu'est ce qu'un état, qu'est ce qu'une politique plus sur ce [post medium](https://medium.com/intro-to-artificial-intelligence/a-brief-explanation-of-state-action-value-function-q-in-rl-ed010ca7d69b)
  * il ne parle que de Q pas de v
* Ce [medium post](https://medium.com/intro-to-artificial-intelligence/relationship-between-state-v-and-action-q-value-function-in-reinforcement-learning-bb9a988c0127) parle de Q et de V, il semble mélanger les 2, il les distingue dans __Relationship between V & Q__
  * la référenec 3 de l'article est intéressante ...

* fin du slide 14
  * le Rsa est le Qpi(s,a) du [dernier post](https://medium.com/intro-to-artificial-intelligence/relationship-between-state-v-and-action-q-value-function-in-reinforcement-learning-bb9a988c0127)

# Slide 16/44
* __Question__ : v(s;Theta) est il égal à Vpi(s) du post ????
  * Vpi(s) est i un scalaire ????

# Slide 17/44
* la ième dérivée représente la dérivée par rapport à la kième composante de Theta

# Slide 23/44
* __Question__ Qu'et ce que Phi(s,a) ? En quoi est ce un vecteur ?????
* __Question__ bas de slide en qui le score est il - espérance possibilité d'un papier ?

# Slide 27/44
* bas très important l'espérance approchée ....

# Slide 31 et 32 / 44
* l'espérance est calculé en parcourant plusieurs trajectoires à partir de Pi(s,a)
* __Question__: Pi(s,a) est elle une matrice de probabilités avec les états en ligne et les actions en colonnes ????
* L'agortihtme est très intéressant
  * _Question (résolue en TP)_: comment calcule t'on PiTheta(s,a)
* Pi(theta) est mis à jour à cahque nouvel épisaode

# Slide 38/44
* __Question__: comment passe t'on du gradient de J qui est une eséprance à un Delat de Thata qui est sans espérance
  * est ce que l'on met à jour à chaque parcours des m samples ?
  * doit on mettre à jour et Theta et w ou bien w est fixé ?
  * Pourquoi a t'on besoinde w pour avoir un Q simple ?

# Slide 41/44
* __Question__: démo à faire pourquoi B(s) ne change rien au gradient de J ?

# Slide 42/44
* __Questio__: pourquoi l'avantag n'est pas r tout court ?

# Sources 
* A new Source [Sergey Levine YouTube Videos](https://www.youtube.com/watch?v=b97H5uz8xkI)