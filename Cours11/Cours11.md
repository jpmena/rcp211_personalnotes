# Références bibliographiques
## La Bible Bishop
* [Bishop Pattern Recognition and Machine Learning free download from Micrsoft](https://www.microsoft.com/en-us/research/people/cmbishop/prml-book/)
  * parce qu'il est ingénieur à Microsoft
* a mis en ligne ses [résultats au format ipnb](https://github.com/gerdm/prml)
* [PRML Notes](https://github.com/vagmcs/PRML/tree/master)
## P Murphy
* [Murphy_Machine_Learning.pdf Livre 1](https://probml.github.io/pml-book/book1.html)
  * fait 45.7 MB
* le code accompgnant le livre fait l'objet d'un [site GitHub](https://github.com/probml)
  * pour le livre 1 cela donne [Livre 1 code](https://github.com/probml/pyprobml/tree/master/notebooks/book1)
## chez moi
* __Bayesian Optimization in Action__ dur [mon dashboard Manning](https://www.manning.com/dashboard)
  * Copyright 2023
  * p 346 et plus solutions aux exercices
* Le code est disponible sur le [GitHub de Kris Nguyen (l'auteur)](https://github.com/KrisNguyen135/bayesian-optimization-in-action)

# 18/43 (1 question)
* cf. p 22 (42/758) of [Bishop Pattern Recognition and Machine Learning free download from Micrsoft](https://www.microsoft.com/en-us/research/people/cmbishop/prml-book/)
* je ne comprends tojours pas la règle de Bayes à 3 variables !!!
* explication du prof
  * $p(b,c) \times p(a|b,c) = p(a,b|c) \times p(c) = p(a, b, c)$
* Q1 Quel lien entre $P(A|B) = \frac{P(B|A) \times P(A)}{P(B)}$ avec:
  * $p(Y, w/X) = p(Y/X,w) \times p(w)$
  * Qui est A et qui est B ?
  * ou bien qui est a qui est b qui est c
## réponse apportée en direct par _marc.lafon@lecnam.net_ en début du TP12:
* $P_{/X}(.) = P(./X)$
* Ici B=Y, A=w
* $P_{/X}(w/Y) = \frac{P_{/X}(Y/w) \times P_{/X}(w)}{P_{/X}(Y)}$
* $P_{/X}(Y)$ ne dépend pas de w donc
  * $P_{/X}(w/Y) \propto P_{/X}(Y/w) \times P_{/X}(w)$
  * ce qui est la même chose que
    * $P(w|\textbf{X,Y}) \propto P(Y|\textbf{X,w}) \times P(w|\textbf{X})$
    * or en entrée du réseau X et w indépendants donc $P(w|\textbf{X}) =P(w)$
    * $P(Y|\textbf{X,w})$ est la sortie de l'équation dans le cas du closed form ou sinon la sortie du réseau de Neurones
* cf. aussi prise de notes
<img src="images/RCP211_TP12_p1.jpg" width="1000px" alt="Schéma équation de Bayes" />

# 20/43 2 questions

## partie manuscrite
* Q2 même question qur Q1: $P(y,w|x^*,D) \propto P(y|x^*,w) \times P(w|D)$
  * où est a, b, c dans une formule de Bayes à trois variables
  * Pourquoi $P(y|x^*,D)$ ne suffit pas ?
# 21/43
* Q3 $w_{MAP} = argmin_w p(w|D)$ d'après le schéma manuscrit c'est plutôt argmax (Définition du $W_{MAP}$: maximum à postériori de W)

# 25/43
* Slide important pour bien poser

# 27/43 (1 Question)
* Q4: quelle différence entre MLE et ML ?
# 28/43 (1 Question)
* [loi de Bernouilli](https://fr.wikipedia.org/wiki/Loi_de_Bernoulli)
* $x_i \in \left\{0,1\right\}$
* Q5: why __P(X ∣p MLE ) = 1 for all futures tosses__

# 34/43
* $\alpha, \beta$ déjà définis 29/43

# 35/43
* Comment je calcule $\Sigma$ et quelle est sa dimension ?
  * réponse p 34/43 et dimentions on 36/43
# 36/43 (1 Question)
* Q6: que dit le $x^*_{min}$

# 38/43 (1 Question)
* Q7: d'où vient la formule de $logp(w|\alpha) = \frac{\alpha}{2} \times w^T \times w$
  * de quel slide (Pourquoi w ne dépend que de $\alpha$ et pas de $\beta$)
