# 10/36
* ML: machine learning
* DL: Deep Learning
* X-AI: Explanable AI
# 12/36 (je suis arrivé à ce moment là)
* saliency: saillance, prépondérance
# 14/36
* le papier [ziegler Fergus](https://arxiv.org/abs/1311.2901)
* la [transposed convolution](https://d2l.ai/chapter_computer-vision/transposed-conv.html)
* Il semble que la transposée d'une RELU soit une RELU
# 15/36
* [what is Global Average Pooling](https://paperswithcode.com/method/global-average-pooling)
* la papier sur les [Class Activation Maps](https://github.com/zhoubolei/CAM)
  * en particulier la fonction [pytorch_CAM.py](https://github.com/zhoubolei/CAM/blob/master/pytorch_CAM.py)
  * bien expliqué fin p2/9 du papier (avec formules)
  * et début page 3/9 où l'on retrouve les images
# 16/36:
* plus $A_{k,i,j}$ est important dans le gradient de $y_c$ plus son coefficient est important
* upsamling (bilinear) [pytorch doc](https://pytorch.org/docs/stable/generated/torch.nn.Upsample.html)
# 17/36
* [Article du Fraunhofer sur LRP](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwi_jZW1ndiDAxVxbKQEHVbhBWoQFnoECBoQAQ&url=https%3A%2F%2Fiphome.hhi.de%2Fsamek%2Fpdf%2FMonXAI19.pdf&usg=AOvVaw0hIuRA34dI6xyrndJRnJVH&opi=89978449)
