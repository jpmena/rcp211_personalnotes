# Origin
* found on the Internet [Lecture8b](https://cme.h-its.org/exelixis/web/teaching/lectures20_21/lecture8b.pdf)
  
# Slide 14/90
* Very good explanation of Xt (states) and the transition Matrix
  
# Slide 15

 * the sum of all values in a row must be 1 __SUMj(pij) = 1__
   * not the sum of values in a column 
* on the Slide 16 at the end a good definition of pij

# Slide 23/90

* Note that the state in t+1 is the list of column

# Slide 25
* the matrix does not change its coefficients with the time

# Slide 27
* we show that transitioning from 0 to 2 id made with P<sup>2</sup>
* on the next slides we generalize and on _Slide 30_ we have a very expressive diagram  
  *  P<sup>2</sup> is only Markov see diagram
## Slide 34
* very general case !
  * __slide 32__  we have four states _A, C, G, T_

## Slide 35 and 36
* We introduce Pi as the probility vectore of size N, in case of _A, C, G, T_ Pi<sub>A<sub> = Pi<sub>1</sub> 
  * probability to start in the State A or the first state

## Slide 37:
* Pitranspose * P is a line vector o N=4 column indiced by j !!!

## Slide 38
* The Pi vector is only good at X0 it is here _transpose(0.2, 0.3, 0.4, 0.1)_
* Sum over the i of X0

## Slide 40
* a goos summary of Pi Pit * P (why? is nicely explained)
  * we take X0 and X1 as line vectors (of N=4 probable states here)
  * Pi is still a column vector

## slide 42 logical generalization
* If all state vectors are line vectors
* X0 ~ transpose(Pi)
* Xt ~ transpose(Pi) * P**t (P**t is t times multiplication of P by itself)

## slide 45/90
* for following a path from state 1 we just need to multiply Pi1 we the individual pij even if it is the same as th matrix multiplication above

## Slidep 48
* Pt and Pu designate the Product of matrix P with itself t times or u times

## The rest is more about markov chains

