# GAN
* for __Generative Antagonist Network__
 * l'[article à l'origine de Goodfellow](https://arxiv.org/pdf/1406.2661v1.pdf)
* Adversarial ou antagonisme vient de la théorie du jeu
* Site proposé sur le Forum par un étudiant [GANLAB](https://poloclub.github.io/ganlab/)
# 3/39
* un réseau de Neurones: fonction non linéaire, paramétrique et différentiable...
* G (Générateur) de paramètre $\theta$
* D (Discriminateur) de paramètre $\phi$
# 6/39
* je ne comprends pas la formule
* la formule est expliquée dans [l'article d'originede GoodFellow](https://arxiv.org/pdf/1406.2661v1.pdf)
  * bas page 2 début page 3
    * *We train D to maximize the probability of assigning the correct label to both training examples and samples from G. We simultaneously train G to minimize log(1 − D(G(z)))*
    * log(1 − D(G(z))) est calculé sur le bruit $p_z$
    * $\mathbb{E}_x \sim p_{data(x)}$ représente l'espérance sur les données réelle pour celles là on veut que D maximise **log(D(x))** (négatif proche de 0)
    * $\mathbb{E}_z \sim p_z(z)$ représente l'espérance sur les données générées là on veut que G minimise **log(1-D(G(z)))** (*D(G(z))* proche de 1, faisant croire que z est une donnée réelle, il trompe D) le log doit tendre vers moins l'infini!!!! D minimise *log(1-D(G(z)))* en faisant en sorte que D(G(z)) tende vers 0
* Cela est bien expliqué dans la partie **Antagonisme** de ce slide 6/39
# 7/39
* Analogie du faussaire intéressante faire croire que D(G(z)) = 1 c'est à dire que $\hat{x} = G(z)$ est une donnée réelle

# 8/39
* le discriminateur (Neural network multicouche) a une sortie unique 0 ou 1
  * en fait toute valeur entre 0 et 1 via une sigmoide $f(x) = \frac{1}{1 + e^{-x}}$
* si on lui passe une entrée générée $\hat{x} \in X$ il doit retourner 0
* si on lui passe une entrée réelle $x \in X$ il doit retourner 1
* de là le schéma du professeur
  * un intervalle 0 ... 1 avec plein de traits près de zéro et plein de traits près de 1 (sorties de la sigmoïde)

# jusqu'à 12/39
* $p_{data(x)} \iff D$ on tire un échantillon de taille m
* $p_z(z) \iff p_z$ on tire un échantillon de la même taille m
* On fait le retour de gradient en 2 fois

# 14/39
* divergence de **Jensen Shannon**
  * [lien wikipedia vers divergence](https://en.wikipedia.org/wiki/Jensen%E2%80%93Shannon_divergence)
  * cette dernière fait appel à la [Kuback Leibner divergence](https://fr.wikipedia.org/wiki/Divergence_de_Kullback-Leibler)
# 16/39
## prise de notes:
* L’objectif n’est pas d’apprendre D mais d’apprendre le meilleur générateur possible. On doit jeter le discriminateur… G = p(x|z) qui  doit le mieux tromper le Discriminateur
* Le discriminateur est construit n’importe comment… D est un punching ball pour le générateur.
## Slide (1 Question):
* D définit une mesure de distance implicite entre les deux distributions
  * (qu’elle tente de maximiser tandis que G tente de la réduire)
* Q1 (montée de gradien) La mise à jour de D pour bien distinguer met elle à jour G pour générer de vraies images (faire échouer G à générer de fausses images)
    * La mise à jour de G (descente de gradient générer de fausses image) met elle à jour D pour bien distinguer les fausses images!
> Non. La mise à jour de G ne porte que sur les paramètres θ de G. Le gradient ∇θ Lg est calculé par rétropropagation dans D puis G, mais seuls les paramètres de G sont mis à jour.
* Le but de G est de rendre non séparables vraies et fausse images ?
> Oui, en modifiant les données générées de sorte à les rendre inséparables pour le discriminateur D.

# 17/39
* 100 z en entrée du génrateur est il  un N(0,I) de dimension 100!
  * cf. TP

# 27/39 coût alternatif
* $gradient(\theta) - log(D_\phi(G_\theta(z)))$
* si trompe mal la log vaut 0 et le gradient de $\theta$ compense
* si trompe bien la log vaut $-\infty$ dong la formule vaut $+\infty$
# 30/39

* la [distance de Wasserstein définie sur ce WIKI](https://fr.wikipedia.org/wiki/Distance_de_Wasserstein)
  * dite aussi distance du cantonnier (Earth Moving distance)
# 32/39
* on a optimisé $G_\theta$ on veut optimiser $D_\phi$ cf. _p 12/39 étape de montée du gradient sur les paramètres $\phi$ du discriminateur_

# 33/39 (1 question)
* Q2: où a t'on vu dans le cours que spéaprer les distribution était le fait de $D_\phi$
  * optimiser $G_\theta$ revenait à les rapprocher ...
  * Ce sont les même p ce qui veut dire que G peut fabriquer des fausses qui ressemblent à des vraies
> C'est l'équation de la fonction objectif du GAN (le jeu minimax) qui formalise le rôle de D et G : D tente de maximiser la fonction objectif, ce qui revient à augmenter les scores des données réelles et diminuer les scores des données générées. G tente de minimiser la fonction objectif. Hors, G n'agit qu'indirectement sur les scores (puisqu'ils sont calculés par D). Donc la seule façon que G a de changer les scores est de produire des données avec un score plus élevé (donc des données classées comme étant « vraies » par le discriminateur).
* $\mathbb{E} \sim pr$ et $\mathbb{E} \sim pz$ valent tous $\frac{1}{m \div 2}$
* [Gulrajani et al](https://arxiv.org/pdf/1704.00028.pdf) p 4/20 algorithme TODO à revoir

# 34/39 (1 question)
* (Q3) comment détermine t'on les features  d'une image par exemple ?
> L'option classique est d'utiliser les activations d'intermédiaires (dernières activations avant la couche de classification) d'un CNN préentraîné sur ImageNet, par exemple le modèle [Inceptionv3](https://keras.io/api/applications/inceptionv3/).

# 39/39 Auxliary classifier GAN (2 questions):
* (Q4 / résolu) $y^r$ et $y^g$ sont les classes c à partir d'exemples réels et générés
  * c'est bien ce que confirme l'article si après
## papier [ACGAN article](https://machinelearningmastery.com/how-to-develop-an-auxiliary-classifier-gan-ac-gan-from-scratch-with-keras/) 1 question
* Il est écrit:
> The discriminator seeks to maximize the probability of correctly classifying real and fake images (LS) and correctly predicting the class label (LC) of a real or fake image (e.g. LS + LC). The generator seeks to minimize the ability of the discriminator to discriminate real and fake images whilst also maximizing the ability of the discriminator predicting the class label of real and fake images (e.g. LC – LS).

* (Q4) Pourquoi le générateur qui lutte contre le discriminateur veut il que ce dernier classe correctement aussi bien les vraies que les fausses images ...
> Le générateur lutte contre le discriminateur mais pas contre le classifieur auxiliaire. On veut donner un conditionnement y au générateur G et que l'image générée soit de la classe y. Donc le classifieur qui prédit y à partir de l'image ne doit pas se tromper ni sur les images réelles, ni sur les images générées.