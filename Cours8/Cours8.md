# notice
> Les réponse du professeur
# 10/45
* on ajoute l'information y à z car ce que l'on veut est $p\theta(x|y,z)$
  * sachant y
* Je ne comprends pas le graphe ...

## je ne comprends pas l'image

# 17/45
* génération conditionnelle, on cherche de s'apporcher de _p(x|y)_
* génère moi un chat sachant que y=chat !!!!

# 19/45
* L'ELBO est le même que pour le cours 7, on rajoute juste le **|y** en plus
* Le KL est toujours positif (admis au Cours 7)
  * cf planche 30/43 du cours 7 : **log pθ (x) ≥ L(θ, φ; x)**

# 22/45
* La matrice des variances/covariance (grand $\Sigma$)

# p 23/45 (Questions au professeur)
* Q1: Comment calcule t'on le $p(y|\tilde{x})$
 * est ce la sortie du décodeur avec en entrée $\tilde{x}$
> $p(y|\tilde{x})$ correspond aux prédictions du modèle discriminatif. Si on note f le classifieur utilisé pour l'Inception Score, alors $p(y|\tilde{x})$ = $f(\tilde{x})$.
* Q2: KL est une log où une somme de Log
  * c'est la somme sur tous les y possibles ?
  * pour l'espérance comment calcule t'on p(x|y)
> Oui. y étant une variable aléatoire qui suit une distribution catégorielle à K classes, la définition de la divergence KL est :
$KL(p(y|x)||p(y)) = \sum_{k=1}^{K} [p(y=k|x) * log[\frac{p(y=k|x)} {p(y=k)}]]$
> L'espérance est assimilée à la moyenne empirique sur le jeu de données généré, donc : IS=$exp(\sum_{i=1}^N KL(p(y|\tilde{x})||p(y)))$

# 26/45 processus autorégressif définition
* t-p >= 0 !
* cas p = 3
  * $X_1$ = c + $\phi1$*X0 + $\epsilon1$
  * $X_2$ = c + $\phi1$*X1 + $\phi2$*X0 + $\epsilon2$
  * $X_3$ = c + $\phi1$*X2 + $\phi2$*X1 + $\phi3$*X0 + $\epsilon3$
  * $X_4$ = c + $\phi1$*X3 + $\phi2$*X2 + $\phi3$*X1 + $\epsilon4$

# 28/45
* La partie modélisation n'est pas simple
* $p(x_1, x_2, ... x_{d-1}, x_d)$ = $p(x_d|x_1, x_2, ... x_{d-1}) \times p(x_1, x_2, ... x_{d-1})$
  * or $p(x_1, x_2, ... x_{d-1})$ = $p(x_{d-1}|x_1, x_2, ... x_{d-2}) \times p(x_1, x_2, ... x_{d-2})$
* le signe **<** veut dire strictement inférieur dans la formule alors que dans le slide d'avant i voulait dire inférieur ou égal
* Q3: Quel lien entre la dimension d des données les indices temporels 1..d
> Sur cette planche, aucun lien entre la dimension d des données et les indices temporels. En revanche, à partir de la planche 30 (12/25), il y a un lien : on modélise une variable aléatoire à d dimensions comme étant un processus autorégressif de longueur d (donc à d pas de temps).
* Q4: on a bien p(x) = $p(x_1, x_2, ..., x_{d-1}, x_d)$ = $p(x_1) \times p(x_2|x_1) \times p(x_3|x_2,x_1) \times ... \times p(x_d|x_{d-1}, x_{d-1}, .., x_2, x_1)$

# 31/45 Exemple
* p de la [Loi de Bernouilli](https://fr.wikipedia.org/wiki/Loi_de_Bernoulli) est ici $p_{\theta_i}(x_i|x_{<i})$ la probabilité conditionnelle pour que $x_i$ vale 1 !!!
## Cas simple:
* $p_{\theta_2}(x_2|x_1)$ = $f_2(x_1)$ = $\sigma(\alpha_0^{(2)} + \alpha_1^{(2)}\times x_1)$
* $p_{\theta_3}(x_3|x_1, x_2)$ = $f_3(x_1, x_2)$ = $\sigma(\alpha_0^{(3)} + \alpha_1^{(3)}\times x_1 + \alpha_2^{(3)}\times x_2)$
* ........................
* $p_{\theta_i}(x_i|x_1, x_2, ..., x_{i-1})$ = $f_i(x_1, x_2, ...., x_{i-1})$ = $\sigma(\alpha_0^{(i)} + \alpha_1^{(i)}\times x_1 + \alpha_2^{(i)}\times x_2 + .... + \alpha_{i-1}^{(i)}\times x_{i-1})$

# 32/45 cas du NADE
## ce qu'est le NADE:
* _Neural Autoregressive Density Estimator_
* [page 3 of journal of machine learning](https://www.jmlr.org/papers/volume17/16-272/16-272.pdf)
  * expressions 2 et 3
  * equation 1 de [page 2 of journal of machine learning](https://www.jmlr.org/papers/volume17/16-272/16-272.pdf) est importante pour tout comprendre!!!
  * dans l'équation 1, __d__ va de 1 à D !!!
### page 2 de l'article:
* o is the order ...
* *Here o<d contains the first d − 1 dimensions in ordering o and xo<d is the corresponding subvector for these dimensions.*
  * _xo<d_ is the subvector {$xo_1, xo_2..., xo_{d-1}$}
* pour comprendre l'expression 5 se référer à [cette autre publication](https://proceedings.mlr.press/v15/bengio11a/bengio11a.pdf)
  * $P(x_i|x_{<i}) = sigm(b_i + W_{i,.} \times sigma(c + W_{.,<i} \times x_{<i}))$
  * avec $W_{i,.}$ la ligne i de W
  * et $ W_{.,<i}$ la sous matrice W (Q5: ne serait-ce pas plutôt de $W^t$) de colonnes de 1 à i-1
> La convention que j'utilise est celle de l'article original du NADE. Le fait de choisir W ou Wᵗ est purement cosmétique (cela revient à changer de convention concernant les activations : sont-elles des vecteurs lignes ou des vecteurs colonnes ?).

# 33/45

* matrices M pour Mask ou masque en français
* la ligne 3 (ou sortie 3) ne peut aller qua'avec les colonnes 1 et 2 ...

# TODO: Terminer la partie auto-régression

# 40/45
* e1, e2, ..., $e_K$ de dimension chacun d (dimension d'un élément z) ?
# 42/45
* uniforme sur [a b] la densité de probabilité est 1 / (b-a) elle est 0 ailleurs.
* déterministe vaut 1 dans une certaine zone et zero ailleurs moyenne sur 1 + 0 = 1
# 43/45 (2 questions)
* $z_e(x)$ signifie résultat de l'encodeur pour x en entrée
* Q4: qu'est ce que *le gradient en entrée du décodeur $D_\theta$*
  * est ce $z_q$ ?
> Le gradient de la fonction objectif par rapport à l'entrée du décodeur, c'est-à-dire $\partial L/\partial z_q$.

* Q5: que signifie stop_gradient que fait cette fonction et qui est __e__ dans
  * ||sg($z_e(x)$) - e$||^2$
  * [small explainatation](https://www.tensorflow.org/api_docs/python/tf/stop_gradient)
> L'opérateur "stop gradient" signifie  que l'on ne rétropropage pas de gradient selon cette variable.

> Dans le VQ-VAE, on calcule une fonction de coût L2 entre z = f(x) et e. Quand on calcule || sg(z) - e ||, cela signifie que l'optimisation se fait seulement pour e : on calcule le gradient par rapport à e, mais on ne veut pas modifier z (z est une "vérité terrain") dans cette équation.

> Inversement, dans || z - sg(e) ||, on cherche à rapprocher z de e, mais on ne veut pas modifier e.


  * Question à poser au gars duTP
# 45/45 (1 question)
* Q6: En quoi Pixel CNN permet de déterminer les z on fait un NADE pour déterminer les z ?
> PixelCNN est un modèle autorégressif. Au milieu du VQ-VAE, on passe de ze à zq en quantifiant les codes z en atomes du codebook. La représentation interne est donc une séquence d'entiers zi = [i₁, i₂, … iₙ].

> Du point de vue de la modélisation générative, le modèle génératif est formé par le décodeur, il s'agit de p(x|zq). Donc pour générer une nouvelle donnée, il faut donner un zq. Hors, on ne connaît pas la distribution p(zq). Au mieux, on connaît pour chaque donnée réelle x la séquence d'entiers zi associée obtenue par encodage + quantification.

> Pour estimer la distribution p(zi), on utilise alors un modèle autorégressif sur les séquences d'entiers zi. Dans le cas des images, les séquences forment en fait une matrice d'entiers, sur laquelle on peut appliquer un modèle autorégressif type PixelCNN. Mais n'importe quel modèle autorégressif pourrait faire l'affaire.